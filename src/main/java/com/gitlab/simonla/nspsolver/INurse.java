package com.gitlab.simonla.nspsolver;

/**
 * Interface to be implemented in classes that represent the nurses, i.e. the entities that are to be scheduled to the {@link IShift}.
 * <br>
 * This interface does not specifies any method as its only use is to declare an existing class as a nurse.
 *
 * @see IShift
 */
public interface INurse {
}
