package com.gitlab.simonla.nspsolver;

import java.util.ArrayList;

/**
 * Class to represent a solution for an instance of the <a href="https://en.wikipedia.org/wiki/Nurse_scheduling_problem">Nurse Scheduling Problem (NSP)</a>, also known as the Nurse Rostering Problem.
 * <br><br>
 * A solution to a Nsp is a schedule specifying which nurse to schedule to which shift.
 * So essentially this class is only a wrapper around the list of {@link com.gitlab.simonla.nspsolver.IShift} which themselves contain the nurses assigned to them.
 * <br>
 * This class may be used as a standalone class or extended to e.g. use an existing class or add some custom functionality.
 *
 * @see IShift
 * @see INurse
 * @see Nsp
 */
public class Solution {

    /**
     * Shifts with nurses assigned to them
     */
    private final ArrayList<IShift> shifts;

    /**
     * Constructs a Solution with the specified shifts
     *
     * @param shifts the shifts with nurses assigned to them
     */
    public Solution(ArrayList<IShift> shifts) {
        this.shifts = shifts;
    }

    /**
     * Returns the schedule, i.e. the list of shifts with nurses assigned to them
     *
     * @return list of shifts with nurses assigned to them
     */
    public ArrayList<IShift> getSchedule() {
        return shifts;
    }
}
