package com.gitlab.simonla.nspsolver;

import java.util.ArrayList;

/**
 * Interface to be implemented in classes that represent the shifts, i.e. the abstract jobs the {@link INurse}s are to be scheduled to.
 * <br>
 * Classes implementing this interface must be capable of storing and returning the list of assigned {@link INurse}s.
 *
 * @see com.gitlab.simonla.nspsolver.INurse
 */
public interface IShift {

    /**
     * Returns the nurses assigned to this shift
     *
     * @return list of {@link INurse}s assigned to this shift
     */
    ArrayList<INurse> getNurses();
}
