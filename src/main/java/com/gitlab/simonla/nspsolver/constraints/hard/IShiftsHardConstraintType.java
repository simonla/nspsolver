package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Represents an operation that accepts a shift, a lowerBound, an upperBound and a solution and returns
 * whether the constraint is fulfilled for the solution.
 * <br><br>
 * This is a functional interface and can therefore be used as the assignment target for a lambda expression or method reference.
 *
 * @see com.gitlab.simonla.nspsolver.constraints.IHardConstraint
 * @see ShiftsHardConstraint
 */
public interface IShiftsHardConstraintType {

    /**
     * Evaluates whether the constraint the shift poses to a solution is fulfilled.
     *
     * @param shift the shift that poses the constraint
     * @param lowerBound a lowerBound that may be used to specify the range of valid solutions
     * @param upperBound an upperBound that may be used to specify the range of valid solutions
     * @param solution the solution to evaluate
     * @return true if the constraint is fulfilled, false otherwise
     */
    boolean isFulfilled(IShift shift, int lowerBound, int upperBound, Solution solution);
}
