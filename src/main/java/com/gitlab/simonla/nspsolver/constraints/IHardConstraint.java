package com.gitlab.simonla.nspsolver.constraints;

import com.gitlab.simonla.nspsolver.Nsp;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Interface to be implemented in classes that represent hard constraints to any Nsp.
 * A hard constraint is a constraint which <b>must</b> be fulfilled by any valid solution.
 * Therefore the hard constraints define the solution space.
 * <br><br>
 * Examples for hard constraints could be:
 * <ul>
 *     <li>number of nurses required for shifts</li>
 *     <li>maximum number of shifts per week</li>
 *     <li>...</li>
 * </ul>
 *
 * @see Nsp
 * @see Solution
 */
public interface IHardConstraint {

    /**
     * Returns whether a given solution fulfills this hard constraint.
     *
     * @param solution solution to evaluate
     * @return true if solution fulfills this hard constraint, false otherwise
     * @see Nsp#isValid(Solution)
     */
    boolean isFulfilled(Solution solution);
}
