package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Represents an operation that accepts two nurses, a boolean describing their relation and a solution and returns
 * the fulfillment rating indicating the quality of this solution regarding this constraint.
 * <br><br>
 * This is a functional interface and can therefore be used as the assignment target for a lambda expression or method reference.
 *
 * @see com.gitlab.simonla.nspsolver.constraints.ISoftConstraint
 * @see NurseRelationSoftConstraint
 */
public interface INurseRelationSoftConstraintType {

    /**
     * Evaluates the fulfillment rating indicating the quality of the solution regarding this constraint between nurse1 and nurse2 specified by coWorking.
     *
     * @param nurse1 first nurse of the nurse-nurse relation
     * @param nurse2 second nurse of the nurse-nurse relation
     * @param coWorking boolean specifying the relation between this two nurses
     * @param solution the solution to evaluate
     * @return double (in range [0, 1]) representing the fulfillment rating of this constraint
     */
    double getFulfillmentRating(INurse nurse1, INurse nurse2, boolean coWorking, Solution solution);
}
