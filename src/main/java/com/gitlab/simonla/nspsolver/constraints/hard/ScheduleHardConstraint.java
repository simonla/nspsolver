package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;

/**
 * Represents a hard constraint the user poses to the schedule.
 * The most common case would be some statistical evaluations and some constraints applying to <b>all</b> nurses / shifts.
 * The boundaries for some value evaluated in this constraint are given by lowerBound and upperBound.
 * These are doubles instead of ints since in this context it often makes sense to evaluate some statistical properties of the schedule.
 * <br><br>
 * This may e.g. be:
 * <ul>
 *     <li>The <a href="https://en.wikipedia.org/wiki/Coefficient_of_variation">coefficient of variation</a> of the nurses workload <b>must</b> be in [0, 0.1]</li>
 *     <li>The <a href="https://en.wikipedia.org/wiki/Coefficient_of_variation">coefficient of variation</a> of the number of nurses assigned to shifts <b>must</b> be in [0, 0.2]</li>
 *     <li>All nurses <b>must</b> be scheduled at least 1 time</li>
 *     <li>All shifts <b>must</b> have at least 2 nurses</li>
 *     <li>Nurses <b>may not</b> work in two consecutive shifts</li>
 *     <li>...</li>
 * </ul>
 *
 * @see IHardConstraint
 * @see IScheduleHardConstraintType
 */
public class ScheduleHardConstraint implements IHardConstraint {
    /**
     * A lower bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final double lowerBound;

    /**
     * An upper bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final double upperBound;

    /**
     * The type of constraint used here, i.e. a function evaluating whether a solution is valid under the constraint represented by this object.
     */
    private final IScheduleHardConstraintType scheduleHardConstraintType;

    /**
     * Constructs a ScheduleHardConstraint the user poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing scheduleHardConstraintType as an evaluation function.
     *
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param scheduleHardConstraintType function checking whether constraint is fulfilled
     */
    public ScheduleHardConstraint(double lowerBound, double upperBound, IScheduleHardConstraintType scheduleHardConstraintType) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.scheduleHardConstraintType = scheduleHardConstraintType;
    }

    /**
     * Constructs a ScheduleHardConstraint the user poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing scheduleHardConstraintType as an evaluation function.
     *
     * @param upperBound an upper bound for some validation range
     * @param scheduleHardConstraintType function checking whether constraint is fulfilled
     */
    public ScheduleHardConstraint(double upperBound, IScheduleHardConstraintType scheduleHardConstraintType) {
        this.lowerBound = 0.0;
        this.upperBound = upperBound;
        this.scheduleHardConstraintType = scheduleHardConstraintType;
    }

    @Override
    public boolean isFulfilled(Solution solution) {
        return this.scheduleHardConstraintType.isFulfilled(lowerBound, upperBound, solution);
    }
}
