package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;

/**
 * Represents a hard constraint a nurse poses to the schedule.
 * The boundaries for some value evaluated in this constraint are given by lowerBound and upperBound.
 * <br><br>
 * This may e.g. be:
 * <ul>
 *     <li>nurse A <b>must</b> have 3 sparse shifts between each shift</li>
 *     <li>nurse B <b>may</b> work at least 1 and at most 4 shifts in this schedule</li>
 *     <li>...</li>
 * </ul>
 *
 * @see IHardConstraint
 * @see INursesHardConstraintType
 */
public class NursesHardConstraint implements IHardConstraint {
    /**
     * The nurse posing this constraint
     */
    private final INurse nurse;

    /**
     * A lower bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final int lowerBound;

    /**
     * An upper bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final int upperBound;

    /**
     * The type of constraint used here, i.e. a function evaluating whether a solution is valid under the constraint represented by this object.
     */
    private final INursesHardConstraintType nursesHardConstraintType;

    /**
     * Constructs a NursesHardConstraint a nurse poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing nursesHardConstraintType as an evaluation function.
     *
     * @param nurse the nurse posing the constraint
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param nursesHardConstraintType function checking whether constraint is fulfilled
     */
    public NursesHardConstraint(INurse nurse, int lowerBound, int upperBound, INursesHardConstraintType nursesHardConstraintType) {
        this.nurse = nurse;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.nursesHardConstraintType = nursesHardConstraintType;
    }

    /**
     * Constructs a NursesHardConstraint a nurse poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing nursesHardConstraintType as an evaluation function.
     *
     * @param nurse the nurse posing the constraint
     * @param upperBound an upper bound for some validation range
     * @param nursesHardConstraintType function checking whether constraint is fulfilled
     */
    public NursesHardConstraint(INurse nurse, int upperBound, INursesHardConstraintType nursesHardConstraintType) {
        this.nurse = nurse;
        this.lowerBound = 0;
        this.upperBound = upperBound;
        this.nursesHardConstraintType = nursesHardConstraintType;
    }

    @Override
    public boolean isFulfilled(Solution solution) {
        return this.nursesHardConstraintType.isFulfilled(nurse, lowerBound, upperBound, solution);
    }
}
