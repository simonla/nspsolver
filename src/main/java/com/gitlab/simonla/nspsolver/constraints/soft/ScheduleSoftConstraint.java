package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;

/**
 * Represents a soft constraint the user poses to the schedule.
 * The most common case would be some statistical evaluations and some constraints applying to <b>all</b> nurses / shifts.
 * The boundaries for some value evaluated in this constraint are given by lowerBound and upperBound.
 * These are doubles instead of ints since in this context it often makes sense to evaluate some statistical properties of the schedule.
 * <br><br>
 * This may e.g. be:
 * <ul>
 *     <li>The <a href="https://en.wikipedia.org/wiki/Coefficient_of_variation">coefficient of variation</a> of the nurses workload <b>should</b> be in [0, 0.1]</li>
 *     <li>The <a href="https://en.wikipedia.org/wiki/Coefficient_of_variation">coefficient of variation</a> of the number of nurses assigned to shifts <b>should</b> be in [0, 0.2]</li>
 *     <li>All nurses <b>should</b> be scheduled at least 1 time</li>
 *     <li>All shifts <b>should</b> have at least 2 nurses</li>
 *     <li>Nurses <b>should not</b> work in two consecutive shifts</li>
 *     <li>...</li>
 * </ul>
 *
 * @see ISoftConstraint
 * @see IScheduleSoftConstraintType
 */
public class ScheduleSoftConstraint extends ASoftConstraint {
    /**
     * A lower bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final double lowerBound;

    /**
     * An upper bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final double upperBound;

    /**
     * The type of constraint used here, i.e. a function evaluating the fulfillment rating of this constraint by a solution.
     */
    private final IScheduleSoftConstraintType scheduleSoftConstraintType;

    /**
     * Constructs a ScheduleSoftConstraint the user poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing nurseRelationSoftConstraintType as an evaluation function with the given weight.
     *
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param scheduleSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     * @param weight the weight associated with this constraint ([0, 1])
     */
    public ScheduleSoftConstraint(double lowerBound, double upperBound, IScheduleSoftConstraintType scheduleSoftConstraintType, double weight) {
        super(weight);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.scheduleSoftConstraintType = scheduleSoftConstraintType;
    }

    /**
     * Constructs a ScheduleSoftConstraint the user poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing nurseRelationSoftConstraintType as an evaluation function with the given weight.
     *
     * @param upperBound an upper bound for some validation range
     * @param scheduleSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     * @param weight the weight associated with this constraint ([0, 1])
     */
    public ScheduleSoftConstraint(double upperBound, IScheduleSoftConstraintType scheduleSoftConstraintType, double weight) {
        super(weight);
        this.lowerBound = 0.0;
        this.upperBound = upperBound;
        this.scheduleSoftConstraintType = scheduleSoftConstraintType;
    }

    /**
     * Constructs a ScheduleSoftConstraint the user poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing nurseRelationSoftConstraintType as an evaluation function with the default weight.
     *
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param scheduleSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     */
    public ScheduleSoftConstraint(double lowerBound, double upperBound, IScheduleSoftConstraintType scheduleSoftConstraintType) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.scheduleSoftConstraintType = scheduleSoftConstraintType;
    }

    /**
     * Constructs a ScheduleSoftConstraint the user poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing nurseRelationSoftConstraintType as an evaluation function with the default weight.
     *
     * @param upperBound an upper bound for some validation range
     * @param scheduleSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     */
    public ScheduleSoftConstraint(double upperBound, IScheduleSoftConstraintType scheduleSoftConstraintType) {
        this.lowerBound = 0.0;
        this.upperBound = upperBound;
        this.scheduleSoftConstraintType = scheduleSoftConstraintType;
    }

    @Override
    public double getFulfillmentRating(Solution solution) {
        return this.scheduleSoftConstraintType.getFulfillmentRating(lowerBound, upperBound, solution);
    }
}
