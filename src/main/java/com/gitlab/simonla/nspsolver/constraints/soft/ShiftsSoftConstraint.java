package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;

/**
 * Represents a soft constraint a shift poses to the schedule.
 * The boundaries for some value evaluated in this constraint are given by lowerBound and upperBound.
 * <br><br>
 * This may e.g. be:
 * <ul>
 *     <li>shift A <b>should</b> have at least 3 nurses</li>
 *     <li>shift B <b>should</b> have exactly 2 nurses with some special skills</li>
 *     <li>...</li>
 * </ul>
 *
 * @see ISoftConstraint
 * @see IShiftsSoftConstraintType
 */
public class ShiftsSoftConstraint extends ASoftConstraint {
    /**
     * The shift posing this constrain
     */
    private final IShift shift;

    /**
     * A lower bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final int lowerBound;

    /**
     * An upper bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final int upperBound;

    /**
     * The type of constraint used here, i.e. a function evaluating the fulfillment rating of this constraint by a solution.
     */
    private final IShiftsSoftConstraintType shiftsSoftConstraintType;

    /**
     * Constructs a ShiftsSoftConstraint a shift poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing nursesSoftConstraintType as an evaluation function with the given weight.
     *
     * @param shift the shift posing the constraint
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param shiftsSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     * @param weight the weight associated with this constraint ([0, 1])
     */
    public ShiftsSoftConstraint(IShift shift, int lowerBound, int upperBound, IShiftsSoftConstraintType shiftsSoftConstraintType, double weight) {
        super(weight);
        this.shift = shift;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.shiftsSoftConstraintType = shiftsSoftConstraintType;
    }

    /**
     * Constructs a ShiftsSoftConstraint a shift poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing nursesSoftConstraintType as an evaluation function with the given weight.
     *
     * @param shift the shift posing the constraint
     * @param upperBound an upper bound for some validation range
     * @param shiftsSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     * @param weight the weight associated with this constraint ([0, 1])
     */
    public ShiftsSoftConstraint(IShift shift, int upperBound, IShiftsSoftConstraintType shiftsSoftConstraintType, double weight) {
        super(weight);
        this.shift = shift;
        this.lowerBound = 0;
        this.upperBound = upperBound;
        this.shiftsSoftConstraintType = shiftsSoftConstraintType;
    }

    /**
     * Constructs a ShiftsSoftConstraint a shift poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing nursesSoftConstraintType as an evaluation function with the default weight.
     *
     * @param shift the shift posing the constraint
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param shiftsSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     */
    public ShiftsSoftConstraint(IShift shift, int lowerBound, int upperBound, IShiftsSoftConstraintType shiftsSoftConstraintType) {
        this.shift = shift;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.shiftsSoftConstraintType = shiftsSoftConstraintType;
    }

    /**
     * Constructs a ShiftsSoftConstraint a shift poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing nursesSoftConstraintType as an evaluation function with the default weight.
     *
     * @param shift the shift posing the constraint
     * @param upperBound an upper bound for some validation range
     * @param shiftsSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     */
    public ShiftsSoftConstraint(IShift shift, int upperBound, IShiftsSoftConstraintType shiftsSoftConstraintType) {
        this.shift = shift;
        this.lowerBound = 0;
        this.upperBound = upperBound;
        this.shiftsSoftConstraintType = shiftsSoftConstraintType;
    }

    @Override
    public double getFulfillmentRating(Solution solution) {
        return this.shiftsSoftConstraintType.getFulfillmentRating(shift, lowerBound, upperBound, solution);
    }
}
