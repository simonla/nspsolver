package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Represents an operation that accepts a nurse, a shift, a boolean describing their relation and a solution and returns
 * whether the constraint is fulfilled for the whole solution.
 * <br><br>
 * This is a functional interface and can therefore be used as the assignment target for a lambda expression or method reference.
 *
 * @see com.gitlab.simonla.nspsolver.constraints.IHardConstraint
 * @see NurseAssignmentHardConstraint
 */
public interface INurseAssignmentHardConstraintType {

    /**
     * Evaluates whether the constraint between the nurse and the shift specified by coWorking is fulfilled for the whole solution.
     *
     * @param nurse the nurse of the nurse-shift relation
     * @param shift the shift of the nurse-shift relation
     * @param assign boolean specifying the relation between the nurse and the shift
     * @param solution the solution to evaluate
     * @return true if the constraint is fulfilled for the whole solution, false otherwise
     */
    boolean isFulfilled(INurse nurse, IShift shift, boolean assign, Solution solution);
}
