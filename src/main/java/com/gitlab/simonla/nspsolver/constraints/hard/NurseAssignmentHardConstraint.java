package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;

/**
 * Represents a hard constraint targeting the relation between a nurse and a shift.
 * The type of relation is specified via the assign parameter.
 *
 * @see IHardConstraint
 * @see INurseAssignmentHardConstraintType
 */
public class NurseAssignmentHardConstraint implements IHardConstraint {
    /**
     * The nurse targeted by this constraint
     */
    private final INurse nurse;

    /**
     * The shift targeted by this constraint
     */
    private final IShift shift;

    /**
     * A parameter describing the nurse-shift relation.
     * This <b>can</b> be used such that:
     * <ul>
     *     <li>true means nurse <b>must</b> be assigned to this shift</li>
     *     <li>false means nurse <b>must not</b> be assigned to this shift</li>
     *     <li>not creating such constraint means there is no constraint (obviously)</li>
     * </ul>
     * Also other approaches are possible. The interpretation relies on the used evaluation function.
     */
    private final boolean assign;

    /**
     * The type of constraint used here, i.e. a function evaluating whether a solution is valid under the constraint represented by this object.
     */
    private final INurseAssignmentHardConstraintType nurseAssignmentHardConstraintType;

    /**
     * Constructs a NurseAssignmentHardConstraint between the given nurse and shift with assign being a parameter to the relation and
     * providing nurseAssignmentHardConstraintType as an evaluation function.
     *
     * @param nurse nurse targeted by this constraint
     * @param shift shift targeted by this constraint
     * @param assign a parameter to the nurse-shift relation
     * @param nurseAssignmentHardConstraintType function checking whether constraint is fulfilled
     */
    public NurseAssignmentHardConstraint(INurse nurse, IShift shift, boolean assign, INurseAssignmentHardConstraintType nurseAssignmentHardConstraintType) {
        this.nurse = nurse;
        this.shift = shift;
        this.assign = assign;
        this.nurseAssignmentHardConstraintType = nurseAssignmentHardConstraintType;
    }

    @Override
    public boolean isFulfilled(Solution solution) {
        return this.nurseAssignmentHardConstraintType.isFulfilled(nurse, shift, assign, solution);
    }
}
