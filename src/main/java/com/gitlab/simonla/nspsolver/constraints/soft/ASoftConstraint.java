package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;

/**
 * Abstract class representing a soft constraint and therefore provides the functionality of setting and getting the weights.
 * Actual soft constraints can extend this class and reuse the weighting functionality.
 */
public abstract class ASoftConstraint implements ISoftConstraint {
    /**
     * The weight, i.e. the importance of this soft constraint (should be in [0, 1])
     */
    private double weight;

    /**
     * Initializes an abstract soft constraint
     *
     * @param weight the weight of this soft constraint
     */
    public ASoftConstraint(double weight) {
        this.weight = weight;
    }

    /**
     * Initializes an abstract soft constraint with default weight 0.5
     */
    public ASoftConstraint() {
        this(0.5);
    }

    /**
     * Returns the weight of this soft constraint restricted to [0, 1].
     *
     * @return the weight if in [0, 1], 0 if weight &lt; 0 and 1 if weight &gt; 1
     */
    @Override
    public double getWeight() {
        if (weight > 1)
            return 1;
        if (weight < 0)
            return 0;
        return weight;
    }

    /**
     * Sets the weight of this soft constraint
     *
     * @param weight the weight to be set. Should be in [0, 1] (i.e.: other values are capped at the boundaries)
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }
}
