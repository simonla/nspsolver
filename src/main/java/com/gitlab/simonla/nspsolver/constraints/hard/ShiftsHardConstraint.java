package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;

/**
 * Represents a hard constraint a shift poses to the schedule.
 * The boundaries for some value evaluated in this constraint are given by lowerBound and upperBound.
 * <br><br>
 * This may e.g. be:
 * <ul>
 *     <li>shift A <b>needs</b> at least 3 nurses</li>
 *     <li>shift B <b>needs</b> exactly 2 nurses with some special skills</li>
 *     <li>...</li>
 * </ul>
 *
 * @see IHardConstraint
 * @see IShiftsHardConstraintType
 */
public class ShiftsHardConstraint implements IHardConstraint {
    /**
     * The shift posing this constrain
     */
    private final IShift shift;

    /**
     * A lower bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final int lowerBound;

    /**
     * An upper bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final int upperBound;

    /**
     * The type of constraint used here, i.e. a function evaluating whether a solution is valid under the constraint represented by this object.
     */
    private final IShiftsHardConstraintType shiftsHardConstraintType;

    /**
     * Constructs a ShiftsHardConstraint a shift poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing shiftsHardConstraintType as an evaluation function.
     *
     * @param shift the shift posing the constraint
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param shiftsHardConstraintType function checking whether constraint is fulfilled
     */
    public ShiftsHardConstraint(IShift shift, int lowerBound, int upperBound, IShiftsHardConstraintType shiftsHardConstraintType) {
        this.shift = shift;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.shiftsHardConstraintType = shiftsHardConstraintType;
    }

    /**
     * Constructs a ShiftsHardConstraint a shift poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing shiftsHardConstraintType as an evaluation function.
     *
     * @param shift the shift posing the constraint
     * @param upperBound an upper bound for some validation range
     * @param shiftsHardConstraintType function checking whether constraint is fulfilled
     */
    public ShiftsHardConstraint(IShift shift, int upperBound, IShiftsHardConstraintType shiftsHardConstraintType) {
        this.shift = shift;
        this.lowerBound = 0;
        this.upperBound = upperBound;
        this.shiftsHardConstraintType = shiftsHardConstraintType;
    }

    @Override
    public boolean isFulfilled(Solution solution) {
        return this.shiftsHardConstraintType.isFulfilled(shift, lowerBound, upperBound, solution);
    }
}
