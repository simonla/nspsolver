package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;

/**
 * Represents a hard constraint targeting the relation between two nurses.
 * The type of relation is specified via the coWorking parameter.
 *
 * @see IHardConstraint
 * @see INurseRelationHardConstraintType
 */
public class NurseRelationHardConstraint implements IHardConstraint {
    /**
     * The first nurse targeted by this constraint
     */
    private final INurse nurse1;

    /**
     * The second nurse targeted by this constraint
     */
    private final INurse nurse2;

    /**
     * A parameter describing the nurse-nurse relation.
     * This <b>can</b> be used such that:
     * <ul>
     *     <li>true means assigning nurse1 to shift implies that nurse2 <b>must</b> be assigned to this shift as well</li>
     *     <li>true means assigning nurse1 to shift implies that nurse2 <b>must not</b> be assigned to this shift as well</li>
     *     <li>not creating such constraint means there is no constraint (obviously)</li>
     * </ul>
     */
    private final boolean coWorking;

    /**
     * The type of constraint used here, i.e. a function evaluating whether a solution is valid under the constraint represented by this object.
     */
    private final INurseRelationHardConstraintType nurseRelationHardConstraintType;

    /**
     * Constructs a NurseRelationHardConstraint between the two given nurses with coWorking being a parameter to the relation and
     * providing nurseRelationHardConstraintType as an evaluation function.
     *
     * @param nurse1 the first nurse targeted by this constraint
     * @param nurse2 the second nurse targeted by this constraint
     * @param coWorking a parameter to the nurse-nurse relation
     * @param nurseRelationHardConstraintType function checking whether constraint is fulfilled
     */
    public NurseRelationHardConstraint(INurse nurse1, INurse nurse2, boolean coWorking, INurseRelationHardConstraintType nurseRelationHardConstraintType) {
        this.nurse1 = nurse1;
        this.nurse2 = nurse2;
        this.coWorking = coWorking;
        this.nurseRelationHardConstraintType = nurseRelationHardConstraintType;
    }

    @Override
    public boolean isFulfilled(Solution solution) {
        return this.nurseRelationHardConstraintType.isFulfilled(nurse1, nurse2, coWorking, solution);
    }
}
