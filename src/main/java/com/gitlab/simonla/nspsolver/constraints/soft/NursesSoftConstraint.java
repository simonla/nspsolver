package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;

/**
 * Represents a soft constraint a nurse poses to the schedule.
 * The boundaries for some value evaluated in this constraint are given by lowerBound and upperBound.
 * <br><br>
 * This may e.g. be:
 * <ul>
 *     <li>nurse A <b>should</b> have 3 sparse shifts between each shift</li>
 *     <li>nurse B <b>should</b> work at least 1 and at most 4 shifts in this schedule</li>
 *     <li>...</li>
 * </ul>
 *
 * @see ISoftConstraint
 * @see INursesSoftConstraintType
 */
public class NursesSoftConstraint extends ASoftConstraint {
    /**
     * The nurse posing this constraint
     */
    private final INurse nurse;

    /**
     * A lower bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final int lowerBound;

    /**
     * An upper bound for some value evaluated in this constraint
     * This <b>can</b> be used to evaluate whether some property evaluated in this constraint is in the given boundaries.
     */
    private final int upperBound;

    /**
     * The type of constraint used here, i.e. a function evaluating the fulfillment rating of this constraint by a solution.
     */
    private final INursesSoftConstraintType nursesSoftConstraintType;

    /**
     * Constructs a NursesSoftConstraint a nurse poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing nursesSoftConstraintType as an evaluation function with the given weight.
     *
     * @param nurse the nurse posing the constraint
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param nursesSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     * @param weight the weight associated with this constraint ([0, 1])
     */
    public NursesSoftConstraint(INurse nurse, int lowerBound, int upperBound, INursesSoftConstraintType nursesSoftConstraintType, double weight) {
        super(weight);
        this.nurse = nurse;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.nursesSoftConstraintType = nursesSoftConstraintType;
    }

    /**
     * Constructs a NursesSoftConstraint a nurse poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing nursesSoftConstraintType as an evaluation function with the given weight.
     *
     * @param nurse the nurse posing the constraint
     * @param upperBound an upper bound for some validation range
     * @param nursesSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     * @param weight the weight associated with this constraint ([0, 1])
     */
    public NursesSoftConstraint(INurse nurse, int upperBound, INursesSoftConstraintType nursesSoftConstraintType, double weight) {
        super(weight);
        this.nurse = nurse;
        this.lowerBound = 0;
        this.upperBound = upperBound;
        this.nursesSoftConstraintType = nursesSoftConstraintType;
    }

    /**
     * Constructs a NursesSoftConstraint a nurse poses to the schedule with lowerBound and upperBound defining a valid range for some evaluated property and
     * providing nursesSoftConstraintType as an evaluation function with the default weight.
     *
     * @param nurse the nurse posing the constraint
     * @param lowerBound a lower bound for some validation range
     * @param upperBound an upper bound for some validation range
     * @param nursesSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     */
    public NursesSoftConstraint(INurse nurse, int lowerBound, int upperBound, INursesSoftConstraintType nursesSoftConstraintType) {
        this.nurse = nurse;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.nursesSoftConstraintType = nursesSoftConstraintType;
    }

    /**
     * Constructs a NursesSoftConstraint a nurse poses to the schedule with lowerBound = 0 and upperBound defining a valid range for some evaluated property and
     * providing nursesSoftConstraintType as an evaluation function with the default weight.
     *
     * @param nurse the nurse posing the constraint
     * @param upperBound an upper bound for some validation range
     * @param nursesSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     */
    public NursesSoftConstraint(INurse nurse, int upperBound, INursesSoftConstraintType nursesSoftConstraintType) {
        this.nurse = nurse;
        this.lowerBound = 0;
        this.upperBound = upperBound;
        this.nursesSoftConstraintType = nursesSoftConstraintType;
    }

    @Override
    public double getFulfillmentRating(Solution solution) {
        return this.nursesSoftConstraintType.getFulfillmentRating(nurse, lowerBound, upperBound, solution);
    }
}
