package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;

/**
 * Represents a soft constraint targeting the relation between a nurse and a shift.
 * The type of relation is specified via the assign parameter.
 *
 * @see ISoftConstraint
 * @see INurseAssignmentSoftConstraintType
 */
public class NurseAssignmentSoftConstraint extends ASoftConstraint {
    /**
     * The nurse targeted by this constraint
     */
    private final INurse nurse;

    /**
     * The shift targeted by this constraint
     */
    private final IShift shift;

    /**
     * A parameter describing the nurse-shift relation.
     * This <b>can</b> be used such that:
     * <ul>
     *     <li>true means nurse <b>should</b> be assigned to this shift</li>
     *     <li>false means nurse <b>should not</b> be assigned to this shift</li>
     *     <li>not creating such constraint means there is no constraint (obviously)</li>
     * </ul>
     * Also other approaches are possible. The interpretation relies on the used evaluation function.
     */
    private final boolean assign;

    /**
     * The type of constraint used here, i.e. a function evaluating the fulfillment rating of this constraint by a solution.
     */
    private final INurseAssignmentSoftConstraintType nurseAssignmentSoftConstraintType;

    /**
     * Constructs a NurseAssignmentSoftConstraint between the given nurse and shift with assign being a parameter to the relation and
     * providing nurseAssignmentSoftConstraintType as an evaluation function with the given weight.
     *
     * @param nurse nurse targeted by this constraint
     * @param shift shift targeted by this constraint
     * @param assign a parameter to the nurse-shift relation
     * @param nurseAssignmentSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     * @param weight the weight associated with this constraint ([0, 1])
     */
    public NurseAssignmentSoftConstraint(INurse nurse, IShift shift, boolean assign, INurseAssignmentSoftConstraintType nurseAssignmentSoftConstraintType, double weight) {
        super(weight);
        this.nurse = nurse;
        this.shift = shift;
        this.assign = assign;
        this.nurseAssignmentSoftConstraintType = nurseAssignmentSoftConstraintType;
    }

    /**
     * Constructs a NurseAssignmentSoftConstraint between the given nurse and shift with assign being a parameter to the relation and
     * providing nurseAssignmentSoftConstraintType as an evaluation function with default weight.
     *
     * @param nurse nurse targeted by this constraint
     * @param shift shift targeted by this constraint
     * @param assign a parameter to the nurse-shift relation
     * @param nurseAssignmentSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     */
    public NurseAssignmentSoftConstraint(INurse nurse, IShift shift, boolean assign, INurseAssignmentSoftConstraintType nurseAssignmentSoftConstraintType) {
        this.nurse = nurse;
        this.shift = shift;
        this.assign = assign;
        this.nurseAssignmentSoftConstraintType = nurseAssignmentSoftConstraintType;
    }

    @Override
    public double getFulfillmentRating(Solution solution) {
        return this.nurseAssignmentSoftConstraintType.getFulfillmentRating(nurse, shift, assign, solution);
    }
}
