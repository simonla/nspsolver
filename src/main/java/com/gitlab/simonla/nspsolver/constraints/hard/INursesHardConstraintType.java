package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Represents an operation that accepts a nurse, a lowerBound, an upperBound and a solution and returns
 * whether the constraint is fulfilled for the solution.
 * <br><br>
 * This is a functional interface and can therefore be used as the assignment target for a lambda expression or method reference.
 *
 * @see com.gitlab.simonla.nspsolver.constraints.IHardConstraint
 * @see NursesHardConstraint
 */
public interface INursesHardConstraintType {

    /**
     * Evaluates whether the constraint the nurse poses to a solution is fulfilled.
     *
     * @param nurse the nurse that poses the constraint
     * @param lowerBound a lowerBound that may be used to specify the range of valid solutions
     * @param upperBound an upperBound that may be used to specify the range of valid solutions
     * @param solution the solution to evaluate
     * @return true if the constraint is fulfilled, false otherwise
     */
    boolean isFulfilled(INurse nurse, int lowerBound, int upperBound, Solution solution);
}
