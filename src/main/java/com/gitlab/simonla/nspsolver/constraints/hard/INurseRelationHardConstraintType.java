package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Represents an operation that accepts two nurses, a boolean describing their relation and a solution and returns
 * whether the constraint is fulfilled for the whole solution.
 * <br><br>
 * This is a functional interface and can therefore be used as the assignment target for a lambda expression or method reference.
 *
 * @see com.gitlab.simonla.nspsolver.constraints.IHardConstraint
 * @see NurseRelationHardConstraint
 */
public interface INurseRelationHardConstraintType {

    /**
     * Evaluates whether the constraint between nurse1 and nurse2 specified by coWorking is fulfilled for the whole solution.
     *
     * @param nurse1 first nurse of the nurse-nurse relation
     * @param nurse2 second nurse of the nurse-nurse relation
     * @param coWorking boolean specifying the relation between this two nurses
     * @param solution the solution to evaluate
     * @return true if the constraint is fulfilled for the whole solution, false otherwise
     */
    boolean isFulfilled(INurse nurse1, INurse nurse2, boolean coWorking, Solution solution);
}
