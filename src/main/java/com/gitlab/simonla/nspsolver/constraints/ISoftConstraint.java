package com.gitlab.simonla.nspsolver.constraints;

import com.gitlab.simonla.nspsolver.Nsp;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Interface to be implemented in classes that represent soft constraints to any Nsp.
 * A soft constraint is a constraint which <b>should</b> be fulfilled but do <b>not has to</b> be.
 * Thereby the set of soft constraints define the fitness, i.e. the relative quality, of solutions.
 * <br><br>
 * Examples for soft constraints could be:
 * <ul>
 *     <li>nurses personal preferences (Nurse A would like to be assigned to a shift with Nurse B, etc.)</li>
 *     <li>it would be desirable to reduce the standard deviation of the number of free shifts between two shifts among all nurses</li>
 *     <li>...</li>
 * </ul>
 *
 * @see Nsp
 * @see Solution
 */
public interface ISoftConstraint {

    /**
     * Returns how much a solution fulfills this soft constraints as a value in the range [0, 1],
     * with 0 corresponding to the worst and 1 the best fulfillment.
     *
     * @param solution solution to evaluate
     * @return double in range [0, 1] representing fulfillment rating
     * @see Nsp#getFitnessRating(Solution)
     */
    double getFulfillmentRating(Solution solution);

    /**
     * Returns the weight, i.e. the importance of this soft constraints as a value in the range [0, 1],
     * with 0 corresponding to the least and 1 the most important constraints.
     * This enables to implement more sophisticated {@link Nsp#getFitnessRating(Solution)} functions and more flexible rating of solutions.
     *
     * @return double in range [0, 1] representing weight
     * @see Nsp#getFitnessRating(Solution)
     */
    double getWeight();
}
