package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;

/**
 * Represents a soft constraint targeting the relation between two nurses.
 * The type of relation is specified via the coWorking parameter.
 *
 * @see ISoftConstraint
 * @see INurseRelationSoftConstraintType
 */
public class NurseRelationSoftConstraint extends ASoftConstraint {
    /**
     * The first nurse targeted by this constraint
     */
    private final INurse nurse1;

    /**
     * The second nurse targeted by this constraint
     */
    private final INurse nurse2;

    /**
     * A parameter describing the nurse-nurse relation.
     * This <b>can</b> be used such that:
     * <ul>
     *     <li>true means assigning nurse1 to shift implies that nurse2 <b>should</b> be assigned to this shift as well</li>
     *     <li>true means assigning nurse1 to shift implies that nurse2 <b>should not</b> be assigned to this shift as well</li>
     *     <li>not creating such constraint means there is no constraint (obviously)</li>
     * </ul>
     */
    private final boolean coWorking;

    /**
     * The type of constraint used here, i.e. a function evaluating the fulfillment rating of this constraint by a solution.
     */
    private final INurseRelationSoftConstraintType nurseRelationSoftConstraintType;

    /**
     * Constructs a NurseRelationSoftConstraint between the two given nurses with coWorking being a parameter to the relation and
     * providing nurseRelationSoftConstraintType as an evaluation function with the given weight.
     *
     * @param nurse1 the first nurse targeted by this constraint
     * @param nurse2 the second nurse targeted by this constraint
     * @param coWorking a parameter to the nurse-nurse relation
     * @param nurseRelationSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     * @param weight the weight associated with this constraint ([0, 1])
     */
    public NurseRelationSoftConstraint(INurse nurse1, INurse nurse2, boolean coWorking, INurseRelationSoftConstraintType nurseRelationSoftConstraintType, double weight) {
        super(weight);
        this.nurse1 = nurse1;
        this.nurse2 = nurse2;
        this.coWorking = coWorking;
        this.nurseRelationSoftConstraintType = nurseRelationSoftConstraintType;
    }

    /**
     * Constructs a NurseRelationSoftConstraint between the two given nurses with coWorking being a parameter to the relation and
     * providing nurseRelationSoftConstraintType as an evaluation function with default weight.
     *
     * @param nurse1 the first nurse targeted by this constraint
     * @param nurse2 the second nurse targeted by this constraint
     * @param coWorking a parameter to the nurse-nurse relation
     * @param nurseRelationSoftConstraintType function evaluating the fulfillment rating of this constraint by a solution
     */
    public NurseRelationSoftConstraint(INurse nurse1, INurse nurse2, boolean coWorking, INurseRelationSoftConstraintType nurseRelationSoftConstraintType) {
        this.nurse1 = nurse1;
        this.nurse2 = nurse2;
        this.coWorking = coWorking;
        this.nurseRelationSoftConstraintType = nurseRelationSoftConstraintType;
    }

    @Override
    public double getFulfillmentRating(Solution solution) {
        return this.nurseRelationSoftConstraintType.getFulfillmentRating(nurse1, nurse2, coWorking, solution);
    }
}
