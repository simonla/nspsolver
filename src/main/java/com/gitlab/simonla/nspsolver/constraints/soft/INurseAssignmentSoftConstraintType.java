package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Represents an operation that accepts a nurse, a shift, a boolean describing their relation and a solution and returns
 * the fulfillment rating indicating the quality of this solution regarding this constraint.
 * <br><br>
 * This is a functional interface and can therefore be used as the assignment target for a lambda expression or method reference.
 *
 * @see com.gitlab.simonla.nspsolver.constraints.ISoftConstraint
 * @see NurseAssignmentSoftConstraint
 */
public interface INurseAssignmentSoftConstraintType {

    /**
     * Evaluates the fulfillment rating indicating the quality of the solution regarding this constraint between the nurse and the shift specified by assign.
     *
     * @param nurse the nurse of the nurse-shift relation
     * @param shift the shift of the nurse-shift relation
     * @param assign boolean specifying the relation between the nurse and the shift
     * @param solution the solution to evaluate
     * @return double (in range [0, 1]) representing the fulfillment rating of this constraint
     */
    double getFulfillmentRating(INurse nurse, IShift shift, boolean assign, Solution solution);
}
