package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.Solution;

/**
 * Represents an operation that accepts a lowerBound, an upperBound and a solution and returns
 * the fulfillment rating indicating the quality of this solution regarding this constraint.
 * <br><br>
 * This is a functional interface and can therefore be used as the assignment target for a lambda expression or method reference.
 *
 * @see com.gitlab.simonla.nspsolver.constraints.ISoftConstraint
 * @see ScheduleSoftConstraint
 */
public interface IScheduleSoftConstraintType {

    /**
     * Evaluates the fulfillment rating indicating the quality of the solution regarding this constraint the schedule poses to a solution.
     *
     * @param lowerBound a lowerBound that may be used to specify the range of valid solutions.
     *                   This is a double since these types of constraints often require statistical evaluations in boundaries.
     * @param upperBound an upperBound that may be used to specify the range of valid solutions.
     *                   This is a double since these types of constraints often require statistical evaluations in boundaries.
     * @param solution the solution to evaluate
     * @return double (in range [0, 1]) representing the fulfillment rating of this constraint
     */
    double getFulfillmentRating(double lowerBound, double upperBound, Solution solution);
}
