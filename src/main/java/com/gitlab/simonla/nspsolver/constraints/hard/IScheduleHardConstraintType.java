package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.Solution;

/**
 * Represents an operation that accepts a lowerBound, an upperBound and a solution and returns
 * whether the constraint is fulfilled for the solution.
 * <br><br>
 * This is a functional interface and can therefore be used as the assignment target for a lambda expression or method reference.
 *
 * @see com.gitlab.simonla.nspsolver.constraints.IHardConstraint
 * @see ScheduleHardConstraint
 */
public interface IScheduleHardConstraintType {

    /**
     * Evaluates whether the constraint the shift poses to a solution is fulfilled.
     *
     * @param lowerBound a lowerBound that may be used to specify the range of valid solutions.
     *                   This is a double since these types of constraints often require statistical evaluations in boundaries.
     * @param upperBound an upperBound that may be used to specify the range of valid solutions.
     *                   This is a double since these types of constraints often require statistical evaluations in boundaries.
     * @param solution the solution to evaluate
     * @return true if the constraint is fulfilled, false otherwise
     */
    boolean isFulfilled(double lowerBound, double upperBound, Solution solution);
}
