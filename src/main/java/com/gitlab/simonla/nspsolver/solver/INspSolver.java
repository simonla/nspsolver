package com.gitlab.simonla.nspsolver.solver;

import com.gitlab.simonla.nspsolver.Nsp;
import com.gitlab.simonla.nspsolver.Solution;

/**
 * Interface to be implemented in classes that implement any Nsp solving algorithm.
 * <br>
 * Classes implementing this interface must be capable of generating a valid {@link Solution} to a given {@link Nsp}
 * according to all its constraints with the nurses and shifts creating this Nsp.
 *
 * @see Nsp
 * @see Solution
 */
public interface INspSolver {

    /**
     * Solve the given Nsp and return a valid solution, i.e. a solution fulfilling all hard constraints.
     *
     * @param nsp nsp to solve
     * @return a valid solution to the given Nsp or null if no solution found
     */
    Solution solveNsp(Nsp nsp);

    /**
     * Solve the given Nsp and return a valid solution, i.e. a solution fulfilling all hard constraints,
     * with a minimun fitness boundary in range [0, 1] which has to be reached from the solution ({@link Nsp#getFitnessRating(Solution)}).
     *
     * @param nsp                nsp to solve
     * @param minFitnessBoundary minimum fitness rating to be reached from the solution
     * @return a valid solution with at least minFitnessBoundary as fitness rating or null if no such solution found
     */
    Solution solveNsp(Nsp nsp, double minFitnessBoundary);
}
