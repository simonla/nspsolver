package com.gitlab.simonla.nspsolver;

import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;
import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;
import com.gitlab.simonla.nspsolver.solver.INspSolver;

import java.util.ArrayList;

/**
 * Class to represent an instance of the <a href="https://en.wikipedia.org/wiki/Nurse_scheduling_problem">Nurse Scheduling Problem (NSP)</a>, also known as the Nurse Rostering Problem.
 * <br><br>
 * A NSP consists of {@link INurse}s which are to be scheduled to {@link IShift}s in an optimal way, while satisfying
 * a set of {@link IHardConstraint}s which <b>must</b> be fulfilled by all valid solutions,
 * and a set of {@link ISoftConstraint}s which <b>should</b> be fulfilled and define the quality of solutions.
 * <br><br>
 * Therefore this class holds all {@link IShift}s, {@link INurse}s, {@link IHardConstraint}s and {@link ISoftConstraint} defining the NSP at hand.
 *
 * @see INurse
 * @see IShift
 * @see IHardConstraint
 * @see ISoftConstraint
 * @see INspSolver
 */
public class Nsp {

    /**
     * Shifts to schedule the nurses to
     */
    private final ArrayList<IShift> shifts;

    /**
     * Nurses to be scheduled
     */
    private final ArrayList<INurse> nurses;

    /**
     * Hard constraints which <b>must</b> be fulfilled by all valid solutions.
     * <p>
     * This can e.g. be the number of nurses, a shift must contain, etc.
     */
    private final ArrayList<IHardConstraint> hardConstraints;

    /**
     * Soft constraints which <b>should</b> be fulfilled by solutions and define the quality of solutions.
     * <p>
     * This can e.g. be personal preferences, etc.
     */
    private final ArrayList<ISoftConstraint> softConstraints;

    /**
     * Constructs a Nsp with the specified shifts, nurses, hardConstraints and softConstraints
     *
     * @param shifts          shifts to schedule nurses to
     * @param nurses          nurses to schedule
     * @param hardConstraints hard constraints defining valid solutions
     * @param softConstraints soft constraints defining the quality of solutions
     */
    public Nsp(ArrayList<IShift> shifts, ArrayList<INurse> nurses, ArrayList<IHardConstraint> hardConstraints, ArrayList<ISoftConstraint> softConstraints) {
        this.shifts = shifts;
        this.nurses = nurses;
        this.hardConstraints = hardConstraints;
        this.softConstraints = softConstraints;
    }

    /**
     * Constructs a Nsp with the specified shifts, nurses and neither hard nor soft constraints
     *
     * @param shifts shifts to schedule nurses to
     * @param nurses nurses to schedule
     */
    public Nsp(ArrayList<IShift> shifts, ArrayList<INurse> nurses) {
        this(shifts, nurses, new ArrayList<>(), new ArrayList<>());
    }

    /**
     * Constructs an empty Nsp
     */
    public Nsp() {
        this(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    }

    /**
     * Returns the list of shifts.
     * <p>
     * The return type is mutable therefore no explicit setter exists.
     *
     * @return list of shifts
     */
    public ArrayList<IShift> getShifts() {
        return shifts;
    }

    /**
     * Returns the list of nurses.
     * <p>
     * The return type is mutable therefore no explicit setter exists.
     *
     * @return list of nurses
     */
    public ArrayList<INurse> getNurses() {
        return nurses;
    }

    /**
     * Returns the list of hard constraints.
     * <p>
     * The return type is mutable therefore no explicit setter exists.
     *
     * @return list of hard constraints
     */
    public ArrayList<IHardConstraint> getHardConstraints() {
        return hardConstraints;
    }

    /**
     * Returns the list of soft constraints.
     * <p>
     * The return type is mutable therefore no explicit setter exists.
     *
     * @return list of soft constraints
     */
    public ArrayList<ISoftConstraint> getSoftConstraints() {
        return softConstraints;
    }

    /**
     * Checks whether the specified solution is valid.
     * A solution is valid iff all hard constraints are fulfilled by this solution.
     *
     * @param solution solution to the nsp to evaluate
     * @return true iff all hard constraints are fulfilled by this solution, false otherwise
     */
    public boolean isValid(Solution solution) {
        for (IHardConstraint hardConstraint : this.hardConstraints)
            if (!hardConstraint.isFulfilled(solution))
                return false;
        return true;
    }

    /**
     * Returns the fitness rating of the specified solution, i.e. a value ([0, 1]) indicating the quality of this solution.
     * <p>
     * This method is likely to be overwritten when using this class since this implementation only implements a default
     * fitness rating calculation strategy, namely the weighted average of the fulfillment ratings of all soft constraints.
     * <p>
     * Overwrite this if any other rating is required. If doing so, you <b>should</b> return:
     * <ul>
     *     <li>0 if the solution is not valid</li>
     *     <li>1 if the solution is valid and no soft constraint given to the Nsp</li>
     *     <li>a value in range [0, 1] indicating fitness rating with 0 corresponding to the worst and 1 the best rating</li>
     * </ul>return 0 if the solution is not valid
     * and 1 if the solution is valid but
     *
     * @param solution solution to the nsp to evaluate
     * @return double in range [0, 1] whereby return value follows specification (see above)
     */
    public double getFitnessRating(Solution solution) {
        if (isValid(solution)) {
            if (!this.softConstraints.isEmpty()) {
                double fitnessRating = 0;
                for (ISoftConstraint softConstraint : this.softConstraints) {
                    fitnessRating += softConstraint.getFulfillmentRating(solution) * softConstraint.getWeight();
                }
                fitnessRating /= this.softConstraints.size();
                return fitnessRating;
            }
            return 1;
        }
        return 0;
    }
}
