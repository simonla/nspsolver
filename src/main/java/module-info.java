module com.gitlab.simonla.nspsolver {
    exports com.gitlab.simonla.nspsolver;
    exports com.gitlab.simonla.nspsolver.constraints;
    exports com.gitlab.simonla.nspsolver.constraints.hard;
    exports com.gitlab.simonla.nspsolver.constraints.soft;
    exports com.gitlab.simonla.nspsolver.solver;
    opens com.gitlab.simonla.nspsolver.constraints;
    opens com.gitlab.simonla.nspsolver.constraints.hard;
    opens com.gitlab.simonla.nspsolver.constraints.soft;
}
