package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NursesSoftConstraintTest {
    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with weight")
    public void whenCallingGetFulfillmentRatingWithWeight_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        int lowerBound = 1;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        INursesSoftConstraintType nursesSoftConstraintType = (n, l, u, sol) -> (n.equals(nurse) ? 1 : 0) + (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        NursesSoftConstraint nursesSoftConstraint = new NursesSoftConstraint(nurse, lowerBound, upperBound, nursesSoftConstraintType, 0.0);

        assertEquals(4.0, nursesSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created")
    public void whenCallingGetFulfillmentRating_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        int lowerBound = 1;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        INursesSoftConstraintType nursesSoftConstraintType = (n, l, u, sol) -> (n.equals(nurse) ? 1 : 0) + (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        NursesSoftConstraint nursesSoftConstraint = new NursesSoftConstraint(nurse, lowerBound, upperBound, nursesSoftConstraintType);

        assertEquals(4.0, nursesSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with weight and with default lower bound")
    public void whenCallingGetFulfillmentRatingWithoutLowerWithWeight_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        int lowerBound = 0;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        INursesSoftConstraintType nursesSoftConstraintType = (n, l, u, sol) -> (n.equals(nurse) ? 1 : 0) + (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        NursesSoftConstraint nursesSoftConstraint = new NursesSoftConstraint(nurse, upperBound, nursesSoftConstraintType, 0.0);

        assertEquals(4.0, nursesSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with default lower bound")
    public void whenCallingGetFulfillmentRatingWithoutLower_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        int lowerBound = 0;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        INursesSoftConstraintType nursesSoftConstraintType = (n, l, u, sol) -> (n.equals(nurse) ? 1 : 0) + (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        NursesSoftConstraint nursesSoftConstraint = new NursesSoftConstraint(nurse, upperBound, nursesSoftConstraintType);

        assertEquals(4.0, nursesSoftConstraint.getFulfillmentRating(solution), 0.001);
    }
}