package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

class NursesHardConstraintTest {
    @Test
    @DisplayName("Lambda called with given fields when calling isFulfilled on object")
    public void whenCallingIsFulfilledOnObject_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        int lowerBound = 1;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        INursesHardConstraintType nursesHardConstraintType = (n, l, u, sol) -> n.equals(nurse) && l == lowerBound && u == upperBound && sol.equals(solution);
        NursesHardConstraint nursesHardConstraint = new NursesHardConstraint(nurse, lowerBound, upperBound, nursesHardConstraintType);

        assertTrue(nursesHardConstraint.isFulfilled(solution));
    }

    @Test
    @DisplayName("Lambda called with given fields when calling isFulfilled on object")
    public void whenCallingIsFulfilledOnObjectWithoutLower_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        int lowerBound = 0;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        INursesHardConstraintType nursesHardConstraintType = (n, l, u, sol) -> n.equals(nurse) && l == lowerBound && u == upperBound && sol.equals(solution);
        NursesHardConstraint nursesHardConstraint = new NursesHardConstraint(nurse, upperBound, nursesHardConstraintType);

        assertTrue(nursesHardConstraint.isFulfilled(solution));
    }
}