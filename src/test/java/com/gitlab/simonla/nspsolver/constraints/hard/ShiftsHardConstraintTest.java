package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ShiftsHardConstraintTest {
    @Test
    @DisplayName("Lambda called with given fields when calling isFulfilled on object")
    public void whenCallingIsFulfilledOnObject_thenPassParametersCorrectlyToLambda() {
        IShift shift = Mockito.mock(IShift.class);
        int lowerBound = 1;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        IShiftsHardConstraintType shiftsHardConstraintType = (s, l, u, sol) -> s.equals(shift) && l == lowerBound && u == upperBound && sol.equals(solution);
        ShiftsHardConstraint shiftsHardConstraint = new ShiftsHardConstraint(shift, lowerBound, upperBound, shiftsHardConstraintType);

        assertTrue(shiftsHardConstraint.isFulfilled(solution));
    }

    @Test
    @DisplayName("Lambda called with given fields when calling isFulfilled on object")
    public void whenCallingIsFulfilledOnObjectWithoutLower_thenPassParametersCorrectlyToLambda() {
        IShift shift = Mockito.mock(IShift.class);
        int lowerBound = 0;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        IShiftsHardConstraintType shiftsHardConstraintType = (s, l, u, sol) -> s.equals(shift) && l == lowerBound && u == upperBound && sol.equals(solution);
        ShiftsHardConstraint shiftsHardConstraint = new ShiftsHardConstraint(shift, upperBound, shiftsHardConstraintType);

        assertTrue(shiftsHardConstraint.isFulfilled(solution));
    }
}