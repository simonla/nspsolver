package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

class NurseRelationHardConstraintTest {
    @Test
    @DisplayName("Lambda called with given fields when calling isFulfilled on object")
    public void whenCallingIsFulfilledOnObject_thenPassParametersCorrectlyToLambda() {
        INurse nurse1 = Mockito.mock(INurse.class);
        INurse nurse2 = Mockito.mock(INurse.class);
        boolean coWorking = true;
        Solution solution = new Solution(new ArrayList<>());
        INurseRelationHardConstraintType nurseRelationHardConstraintType = (n1, n2, co, sol) -> n1.equals(nurse1) && n2.equals(nurse2) && co == coWorking && sol.equals(solution);
        NurseRelationHardConstraint nurseRelationHardConstraint = new NurseRelationHardConstraint(nurse1, nurse2, coWorking, nurseRelationHardConstraintType);

        assertTrue(nurseRelationHardConstraint.isFulfilled(solution));
    }
}