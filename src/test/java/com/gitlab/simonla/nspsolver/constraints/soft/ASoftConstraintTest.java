package com.gitlab.simonla.nspsolver.constraints.soft;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ASoftConstraintTest {

    @Test
    void whenSettingWeightTooHigh_thenReturn1Instead() {
        ASoftConstraint softConstraint = Mockito.mock(ASoftConstraint.class, Mockito.withSettings().useConstructor(1.2).defaultAnswer(Mockito.CALLS_REAL_METHODS));

        assertEquals(1.0, softConstraint.getWeight(), 0.001);
    }

    @Test
    void whenSettingWeightTooLow_thenReturn0Instead() {
        ASoftConstraint softConstraint = Mockito.mock(ASoftConstraint.class, Mockito.withSettings().useConstructor(-0.5).defaultAnswer(Mockito.CALLS_REAL_METHODS));

        assertEquals(0.0, softConstraint.getWeight(), 0.001);
    }

    @Test
    void whenInitializingNoWeight_thenSet05Instead() {
        ASoftConstraint softConstraint = Mockito.mock(ASoftConstraint.class, Mockito.withSettings().useConstructor().defaultAnswer(Mockito.CALLS_REAL_METHODS));

        assertEquals(0.5, softConstraint.getWeight(), 0.001);
    }
}