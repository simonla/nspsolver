package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ScheduleHardConstraintTest {
    @Test
    @DisplayName("Lambda called with given fields when calling isFulfilled on object")
    public void whenCallingIsFulfilledOnObject_thenPassParametersCorrectlyToLambda() {
        double lowerBound = 1.0;
        double upperBound = 2.0;
        Solution solution = new Solution(new ArrayList<>());
        IScheduleHardConstraintType scheduleHardConstraintType = (l, u, sol) -> l == lowerBound && u == upperBound && sol.equals(solution);
        ScheduleHardConstraint scheduleHardConstraint = new ScheduleHardConstraint(lowerBound, upperBound, scheduleHardConstraintType);

        assertTrue(scheduleHardConstraint.isFulfilled(solution));
    }

    @Test
    @DisplayName("Lambda called with given fields when calling isFulfilled on object")
    public void whenCallingIsFulfilledOnObjectWithoutLower_thenPassParametersCorrectlyToLambda() {
        double lowerBound = 0.0;
        double upperBound = 2.0;
        Solution solution = new Solution(new ArrayList<>());
        IScheduleHardConstraintType scheduleHardConstraintType = (l, u, sol) -> l == lowerBound && u == upperBound && sol.equals(solution);
        ScheduleHardConstraint scheduleHardConstraint = new ScheduleHardConstraint(upperBound, scheduleHardConstraintType);

        assertTrue(scheduleHardConstraint.isFulfilled(solution));
    }
}