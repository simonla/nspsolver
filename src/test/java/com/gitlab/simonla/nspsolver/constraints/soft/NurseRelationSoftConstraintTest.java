package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NurseRelationSoftConstraintTest {
    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object")
    public void whenCallingGetFulfillmentRating_thenPassParametersCorrectlyToLambda() {
        INurse nurse1 = Mockito.mock(INurse.class);
        INurse nurse2 = Mockito.mock(INurse.class);
        boolean coWorking = true;
        Solution solution = new Solution(new ArrayList<>());
        INurseRelationSoftConstraintType nurseRelationSoftConstraintType = (n1, n2, co, sol) -> (n1.equals(nurse1) ? 1 : 0) + (n2.equals(nurse2) ? 1 : 0) + (co == coWorking ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        NurseRelationSoftConstraint nurseRelationSoftConstraint = new NurseRelationSoftConstraint(nurse1, nurse2, coWorking, nurseRelationSoftConstraintType);

        assertEquals(4.0, nurseRelationSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with weight")
    public void whenCallingGetFulfillmentRatingWithWeight_thenPassParametersCorrectlyToLambda() {
        INurse nurse1 = Mockito.mock(INurse.class);
        INurse nurse2 = Mockito.mock(INurse.class);
        boolean coWorking = true;
        Solution solution = new Solution(new ArrayList<>());
        INurseRelationSoftConstraintType nurseRelationSoftConstraintType = (n1, n2, co, sol) -> (n1.equals(nurse1) ? 1 : 0) + (n2.equals(nurse2) ? 1 : 0) + (co == coWorking ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        NurseRelationSoftConstraint nurseRelationSoftConstraint = new NurseRelationSoftConstraint(nurse1, nurse2, coWorking, nurseRelationSoftConstraintType, 0.0);

        assertEquals(4.0, nurseRelationSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

}