package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NurseAssignmentSoftConstraintTest {

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object")
    public void whenCallingGetFulfillmentRating_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        IShift shift = Mockito.mock(IShift.class);
        boolean assign = true;
        Solution solution = new Solution(new ArrayList<>());
        INurseAssignmentSoftConstraintType nurseAssignmentSoftConstraintType = (n, s, as, sol) -> (n.equals(nurse) ? 1 : 0) + (s.equals(shift) ? 1 : 0) + (as == assign ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        NurseAssignmentSoftConstraint nurseAssignmentSoftConstraint = new NurseAssignmentSoftConstraint(nurse, shift, assign, nurseAssignmentSoftConstraintType);

        assertEquals(4.0, nurseAssignmentSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with weight")
    public void whenCallingGetFulfillmentRatingWithWeight_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        IShift shift = Mockito.mock(IShift.class);
        boolean assign = true;
        Solution solution = new Solution(new ArrayList<>());
        INurseAssignmentSoftConstraintType nurseAssignmentSoftConstraintType = (n, s, as, sol) -> (n.equals(nurse) ? 1 : 0) + (s.equals(shift) ? 1 : 0) + (as == assign ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        NurseAssignmentSoftConstraint nurseAssignmentSoftConstraint = new NurseAssignmentSoftConstraint(nurse, shift, assign, nurseAssignmentSoftConstraintType, 0.0);

        assertEquals(4.0, nurseAssignmentSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

}