package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ShiftsSoftConstraintTest {
    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with weight")
    public void whenCallingGetFulfillmentRatingWithWeight_thenPassParametersCorrectlyToLambda() {
        IShift shift = Mockito.mock(IShift.class);
        int lowerBound = 1;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        IShiftsSoftConstraintType shiftsSoftConstraintType = (n, l, u, sol) -> (n.equals(shift) ? 1 : 0) + (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        ShiftsSoftConstraint shiftsSoftConstraint = new ShiftsSoftConstraint(shift, lowerBound, upperBound, shiftsSoftConstraintType, 0.0);

        assertEquals(4.0, shiftsSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created")
    public void whenCallingGetFulfillmentRating_thenPassParametersCorrectlyToLambda() {
        IShift shift = Mockito.mock(IShift.class);
        int lowerBound = 1;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        IShiftsSoftConstraintType shiftsSoftConstraintType = (n, l, u, sol) -> (n.equals(shift) ? 1 : 0) + (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        ShiftsSoftConstraint shiftsSoftConstraint = new ShiftsSoftConstraint(shift, lowerBound, upperBound, shiftsSoftConstraintType);

        assertEquals(4.0, shiftsSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with weight and with default lower bound")
    public void whenCallingGetFulfillmentRatingWithoutLowerWithWeight_thenPassParametersCorrectlyToLambda() {
        IShift shift = Mockito.mock(IShift.class);
        int lowerBound = 0;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        IShiftsSoftConstraintType shiftsSoftConstraintType = (n, l, u, sol) -> (n.equals(shift) ? 1 : 0) + (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        ShiftsSoftConstraint shiftsSoftConstraint = new ShiftsSoftConstraint(shift, upperBound, shiftsSoftConstraintType, 0.0);

        assertEquals(4.0, shiftsSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with default lower bound")
    public void whenCallingGetFulfillmentRatingWithoutLower_thenPassParametersCorrectlyToLambda() {
        IShift shift = Mockito.mock(IShift.class);
        int lowerBound = 0;
        int upperBound = 2;
        Solution solution = new Solution(new ArrayList<>());
        IShiftsSoftConstraintType shiftsSoftConstraintType = (n, l, u, sol) -> (n.equals(shift) ? 1 : 0) + (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        ShiftsSoftConstraint shiftsSoftConstraint = new ShiftsSoftConstraint(shift, upperBound, shiftsSoftConstraintType);

        assertEquals(4.0, shiftsSoftConstraint.getFulfillmentRating(solution), 0.001);
    }
}