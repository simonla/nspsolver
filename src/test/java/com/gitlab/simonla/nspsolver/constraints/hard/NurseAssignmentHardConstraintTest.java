package com.gitlab.simonla.nspsolver.constraints.hard;

import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

class NurseAssignmentHardConstraintTest {

    @Test
    @DisplayName("Lambda called with given fields when calling isFulfilled on object")
    public void whenCallingIsFulfilledOnObject_thenPassParametersCorrectlyToLambda() {
        INurse nurse = Mockito.mock(INurse.class);
        IShift shift = Mockito.mock(IShift.class);
        boolean assign = true;
        Solution solution = new Solution(new ArrayList<>());
        INurseAssignmentHardConstraintType nurseAssignmentHardConstraintType = (n, s, as, sol) -> n.equals(nurse) && s.equals(shift) && as == assign && sol.equals(solution);
        NurseAssignmentHardConstraint nurseAssignmentHardConstraint = new NurseAssignmentHardConstraint(nurse, shift, assign, nurseAssignmentHardConstraintType);

        assertTrue(nurseAssignmentHardConstraint.isFulfilled(solution));
    }
}