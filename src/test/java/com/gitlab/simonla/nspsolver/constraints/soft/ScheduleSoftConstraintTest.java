package com.gitlab.simonla.nspsolver.constraints.soft;

import com.gitlab.simonla.nspsolver.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ScheduleSoftConstraintTest {
    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with weight")
    public void whenCallingGetFulfillmentRatingWithWeight_thenPassParametersCorrectlyToLambda() {
        double lowerBound = 1.0;
        double upperBound = 2.0;
        Solution solution = new Solution(new ArrayList<>());
        IScheduleSoftConstraintType scheduleSoftConstraintType = (l, u, sol) -> (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        ScheduleSoftConstraint scheduleSoftConstraint = new ScheduleSoftConstraint(lowerBound, upperBound, scheduleSoftConstraintType, 0.0);

        assertEquals(3.0, scheduleSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created")
    public void whenCallingGetFulfillmentRating_thenPassParametersCorrectlyToLambda() {
        double lowerBound = 1.0;
        double upperBound = 2.0;
        Solution solution = new Solution(new ArrayList<>());
        IScheduleSoftConstraintType scheduleSoftConstraintType = (l, u, sol) -> (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        ScheduleSoftConstraint scheduleSoftConstraint = new ScheduleSoftConstraint(lowerBound, upperBound, scheduleSoftConstraintType);

        assertEquals(3.0, scheduleSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with weight and with default lower bound")
    public void whenCallingGetFulfillmentRatingWithoutLowerWithWeight_thenPassParametersCorrectlyToLambda() {
        double lowerBound = 0.0;
        double upperBound = 2.0;
        Solution solution = new Solution(new ArrayList<>());
        IScheduleSoftConstraintType scheduleSoftConstraintType = (l, u, sol) -> (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        ScheduleSoftConstraint scheduleSoftConstraint = new ScheduleSoftConstraint(upperBound, scheduleSoftConstraintType, 0.0);

        assertEquals(3.0, scheduleSoftConstraint.getFulfillmentRating(solution), 0.001);
    }

    @Test
    @DisplayName("Lambda called with given fields when calling getFulfillmentRating on object created with default lower bound")
    public void whenCallingGetFulfillmentRatingWithoutLower_thenPassParametersCorrectlyToLambda() {
        double lowerBound = 0.0;
        double upperBound = 2.0;
        Solution solution = new Solution(new ArrayList<>());
        IScheduleSoftConstraintType scheduleSoftConstraintType = (l, u, sol) -> (l == lowerBound ? 1 : 0) + (u == upperBound ? 1 : 0) + (sol.equals(solution) ? 1 : 0);
        ScheduleSoftConstraint scheduleSoftConstraint = new ScheduleSoftConstraint(upperBound, scheduleSoftConstraintType);

        assertEquals(3.0, scheduleSoftConstraint.getFulfillmentRating(solution), 0.001);
    }
}