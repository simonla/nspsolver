package com.gitlab.simonla.nspsolver;

import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;
import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class NspTest {

    @Test
    @DisplayName("Solution valid when no hard constraints exist")
    public void whenNoHardConstraints_thenValid() {
        Nsp nsp = new Nsp();
        Solution solution = Mockito.mock(Solution.class);

        assertTrue(nsp.isValid(solution));
    }

    @Test
    @DisplayName("Solution invalid when at least one hard constraint not fulfilled")
    public void whenAtLeastOneHardConstraintNotFulfilled_thenInvalid() {
        Nsp nsp = new Nsp();
        Solution solution = Mockito.mock(Solution.class);
        IHardConstraint hardConstraint = Mockito.mock(IHardConstraint.class);
        when(hardConstraint.isFulfilled(solution)).thenReturn(false);
        nsp.getHardConstraints().add(hardConstraint);

        assertFalse(nsp.isValid(solution));
    }

    @Test
    @DisplayName("Solution valid when all hard constraints fulfilled")
    public void whenAllHardConstraintsFulfilled_thenValid() {
        Nsp nsp = new Nsp();
        Solution solution = Mockito.mock(Solution.class);
        IHardConstraint hardConstraint1 = Mockito.mock(IHardConstraint.class);
        when(hardConstraint1.isFulfilled(solution)).thenReturn(true);
        IHardConstraint hardConstraint2 = Mockito.mock(IHardConstraint.class);
        when(hardConstraint2.isFulfilled(solution)).thenReturn(true);
        nsp.getHardConstraints().add(hardConstraint1);
        nsp.getHardConstraints().add(hardConstraint2);

        assertTrue(nsp.isValid(solution));
    }

    @Test
    @DisplayName("Fitness rating 0 when solution not valid")
    public void whenNotValid_thenFitnessRatingZero() {
        Nsp nsp = new Nsp();
        Solution solution = Mockito.mock(Solution.class);
        IHardConstraint hardConstraint = Mockito.mock(IHardConstraint.class);
        when(hardConstraint.isFulfilled(solution)).thenReturn(false);
        nsp.getHardConstraints().add(hardConstraint);

        assertEquals(0, nsp.getFitnessRating(solution), 0.001);
    }

    @Test
    @DisplayName("Fitness rating 1 when solution valid and no soft constraints")
    public void whenValidAndNoSoftConstraints_thenFitnessRatingOne() {
        Nsp nsp = new Nsp();
        Solution solution = Mockito.mock(Solution.class);
        IHardConstraint hardConstraint = Mockito.mock(IHardConstraint.class);
        when(hardConstraint.isFulfilled(solution)).thenReturn(true);
        nsp.getHardConstraints().add(hardConstraint);

        assertEquals(1, nsp.getFitnessRating(solution), 0.001);
    }

    @Test
    @DisplayName("Fitness rating matches weighted average of fulfilment rating when solution valid and soft constraints exist")
    public void whenValidAndSoftConstraints_thenFitnessRatingWeightedFulfilmentRatingAverage() {
        Nsp nsp = new Nsp();
        Solution solution = Mockito.mock(Solution.class);
        IHardConstraint hardConstraint = Mockito.mock(IHardConstraint.class);
        when(hardConstraint.isFulfilled(solution)).thenReturn(true);
        nsp.getHardConstraints().add(hardConstraint);

        ISoftConstraint softConstraint1 = Mockito.mock(ISoftConstraint.class);
        when(softConstraint1.getFulfillmentRating(solution)).thenReturn(0.23);
        when(softConstraint1.getWeight()).thenReturn(0.95);
        nsp.getSoftConstraints().add(softConstraint1);
        ISoftConstraint softConstraint2 = Mockito.mock(ISoftConstraint.class);
        when(softConstraint2.getFulfillmentRating(solution)).thenReturn(0.8);
        when(softConstraint2.getWeight()).thenReturn(0.4);
        nsp.getSoftConstraints().add(softConstraint2);
        ISoftConstraint softConstraint3 = Mockito.mock(ISoftConstraint.class);
        when(softConstraint3.getFulfillmentRating(solution)).thenReturn(0.6);
        when(softConstraint3.getWeight()).thenReturn(0.5);
        nsp.getSoftConstraints().add(softConstraint3);

        assertEquals((0.23 * 0.95 + 0.8 * 0.4 + 0.6 * 0.5) / 3, nsp.getFitnessRating(solution), 0.001);
    }
}