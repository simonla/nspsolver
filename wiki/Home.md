This is the Wiki to the NSPSolver project. 
*Even though some releases are published already, this project is still in its initial state!*

_In case of questions / bugs / feature requests do not hesitate to submit a corresponding [issue](https://gitlab.com/simonla/nspsolver/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)!_

## Links

- **Javadocs:** View the [javadocs](https://simonla.gitlab.io/nspsolver) of the current release on Gitlab Pages
- **Releases:** View all [releases and changelogs](https://gitlab.com/simonla/nspsolver/-/releases)
- **Packages:** Search and download [maven packages](https://gitlab.com/simonla/nspsolver/-/packages) releases

## General Overview

This project implements a [maven](https://maven.apache.org/) package to solve the [**Nurse Scheduling Problem (NSP)**](https://en.wikipedia.org/wiki/Nurse_scheduling_problem), also known as the **Nurse Rostering Problem**.

This NSP is an [operations research](https://en.wikipedia.org/wiki/Operations_research) problem of finding an optimal way to assign **nurses** to **shifts**.
In most cases instances of this problem define a set of constraints that must / should be fulfilled by solutions. 
These can be differentiated into **hard constraints** which all valid solutions must follow, 
and **soft constraints** which define some desirable but not required constraints and thereby the relative quality of valid solutions.

As such the problem is **applicable to a large set of real-life use cases**, since 
- **nurses** can be seen as a general type of entities which has to be scheduled
- **shifts** can be seen as a general type of jobs the entities have to be scheduled to
- **hard** and **soft constraints** define a really high-level concept of constraints to this problem

Examples where this problem might be applicable, are (class)room scheduling, employee scheduling, job scheduling, etc.

## Quick Start

Please refer to the [Quick Start Guide](Quick-Start).

## Setup

Please refer to the [Setup Guide](Setup).

## Versions

[![version](https://simonla.gitlab.io/nspsolver/version.svg)](https://gitlab.com/simonla/nspsolver/-/releases/)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

This project uses automated [semantic versioning](https://semver.org/lang/de/). 
Therefore releases are created based on [conventional commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/#specification).

That means all versions are in the following format: MAJOR.MINOR.PATCH\
Thereby the MAJOR version increases when there are some backward-incompatible changes (which are documented in the Changelog), 
MINOR version increases when new features are introduced and an increasing of the PATCH
version indicates bug fixes.\
That means that updates _not increasing_ the major version should always be safe to perform with existing codebases.
