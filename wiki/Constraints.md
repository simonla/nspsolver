## Concept

### Hard vs Soft Constraints

In general constraints define the **solution space** of the given NSP and indicate the **quality** of a given solution.
We call the former **hard** and the latter **soft** constraints:

#### Hard Constraints

Hard constraints are those constraints which only have **two states**: fulfilled or not fulfilled.
Any schedule not fulfilling any hard constraint is invalid and needs correction.
Thereby hard constraints **define the solution space**, i.e. define which solutions are valid and which are not.

#### Soft Constraints

Soft constraints are those constraints which may be fulfilled to **some degree** only.
That means, a schedule could violate all soft constraints and still be a valid solution, just probably a bad one.
Thereby soft constraints **define the quality of a solution**, i.e. the more soft constraints a solution fulfills, and the better each of them is fulfilled, the higher is the quality of the analysed schedule.
These quality evaluation can be used to compare multiple valid solutions and select the best.

### Who poses a constraint?

Another way of thinking about constraints is to think about **who poses** this constraint **to whom**?
From this point of view we can define five general types of constraints:

#### 1. Nurse Relation constraints

These constraints describe the relation between two or more nurses.
So in this case **nurses** pose constraints to other **nurses**.
In general these constraints specify whether two nurses should / must or should not / must not be scheduled to the same shift.
Example use cases could be:

- take friendship into account
- schedule apprentices with experienced workers
- indicate whether some resource may be used by the same entity (e.g. schedule either a blackboard and chalk or a beamer to a classroom but not chalk and beamer without a blackboard)
- ...

#### 2. Nurse Assigment constraints

These constraints describe the relation between a shift and a nurse. 
So in this case **nurses** pose constraints to **shifts**.
In general this means that these constraints indicate whether a nurse should / must or should not / must not be scheduled to a given shift. 
Example use cases could be:

- take vacations into account
- respect workers shift preferences (e.g. someone does not want to / may not work at night)
- indicate whether some resource may be used on some entity (e.g. when scheduling computing power and prioritizing processes)
- indicating whether an entity fits into some other (e.g. when scheduling goods to containers)
- ...

If these constraints apply to many / all shifts / nurses, it may be easier to use one nurses / shifts / schedule constraint instead of several nurse assigment constraints.

#### 3. Nurses Constraints

These constraints describe the needs and wishes of nurses. 
So in this case **nurses** pose constraints to the **schedule**.
In general this means that these constraints target the schedule itself directly and define some quality features or rules which must be followed.
Example use cases could be:

- take vacations into account
- respect workers shift preferences (e.g. someone does not want to / may not work at night)
- respect some boundaries for the number of shifts a nurse may work in a schedule
- ...

#### 4. Shifts Constraints

These constraints describe the requirements of the shifts.
So in this case **shifts** pose constraints to the **schedule**.
In general this means that these constraints target the schedule itself directly and define some quality features or rules which must be followed.
Example use cases could be:

- define the minimum / maximum number of nurses required for a shift
- define that a schedule with as many nurses per shifts / the least standard deviation of the number of nurses on each shift / ... is better than one with lesser
- pay attention to some special requirements a shift has (e.g. require a senior nurse / ...)
- ...

#### 5. Schedule Constraints

These constraints describe requirements on the schedule.
So in this case **you** pose constraints to the **schedule**.
In general this means that these constraints target all nurses / shifts of the schedule at the same time and define some quality features or rules which must be followed.
Example use cases could be:

- statistical evaluations on the quality of a solution, e.g.:
  - coefficient of variation of the nurses workload
  - coefficient of variation of the number of nurses assigned to shifts
  - ...
- some constraints applying to all shifts / nurses, e.g.:
  - no nurse may work in 3 subsequent shifts
  - all nurses must be scheduled at least 1 time
  - all shifts need at least 1 nurse
  - ...
- everything else not matching any of the other shifts
- ...

## Programmatic realisation

The structure laid out beforehand can be found in the packages class structure. 
That means there exist two interfaces: `IHardConstraint` and `ISoftConstraint` which **must** be implemented by all classes representing a hard / soft constraint.\
To save some time and make this a bit easier there also exist five classes for soft and five for hard constraints, representing the presented types of constraints (e.g. `NursesHardConstraint`).
These classes implement the according interfaces and provide a constructor, which takes some arguments depending on the type of constraint, and a type of constraint (e.g. `INursesHardConstraintType`).\
These interfaces are **functional interfaces** - that means you can pass **lambdas** to these classes as demonstrated in the [Quick-Start Guide](Quick-Start).

So there are multiple possibilities of creating constraints:

1. Implement the `IHardConstraint` / `ISoftConstraint` interface in any class you like and use that as a hard / soft constraint.
2. Implement the `I...HardConstraintType` / `I...SoftConstraintType` interface in any class you like (i.e. provide a class implementing the evaluation function for this constraint) and pass an instance of this class to the constructor of one of the predefined classes.
3. Use one of the predefined classes and provide a lambda expression as an evaluation function.
4. _Use one of the predefined constraints (will be available in a future release)_