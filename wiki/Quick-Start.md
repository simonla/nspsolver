## Problem definition

_When you know what the NSP is and how it can be applied to your problem, you may safely skip this section._

The first step to use this package to systematically solve your problem at hand is to think about how to structure your problem
in a way that it matches the definition of a NSP used in this package.

### Nurses & Shifts

In this project the terms **nurse** and **shift** always refers to these objects as specified below.
So whenever there is talk about nurses or shifts think of these as being general terms which can apply to various problems and mean
some different things in different contexts.

#### What are your "nurses"?

In the definition of a NSP **nurses** are the **entities to be scheduled**. 
This means that nurses are the resources fulfilling the needs of shifts.
So first you should be clear about which objects of your problem are the nurses equivalent. 
This may e.g. be

- persons (workers, customers, pupils, ...)
- tasks (computational problems, meetings, lessons, ...)
- resources (computing capacity, objects with limited usage, time slots, transport capacity, ...)
- ...

The nurses may have some properties / skills / constraints / wishes / requirements / ...
All these factors are modeled via _constraints_ (see below).

#### What are your "shifts"?

In the definition of a NSP **shifts** are the **entities the nurses are scheduled to**.
This means that shifts are the entities, that require nurses to be fulfilled.
So first you should be clear about which objects of your problem are the shifts equivalent.
This may e.g. be

- jobs (working shifts, tasks, ...)
- resources (computing capacity, objects with limited usage, time slots, transport capacity, ...)
- rooms (meeting rooms, bedrooms, classrooms, ...)
- persons (workers, customers, pupils, ...)
- ...

The shifts may have some properties / constraints / requirements / ...
All these factors are modeled via _constraints_ (see below).

#### A word on nurses and shifts

As can be seen above, depending on the problem the same object may be a shift or a nurse or both at the same time. 
This is caused by the broad definition of the terms in this project. 
For a deeper understanding of the used terms you may think about the following examples:

- _A hospital wants to schedule nurses to shifts:_
  
  The shifts pose requirements on the nurses (number, skills, ...) and the nurses have their own needs and wishes.
  
- _A school wants to schedule lessons to classroom usage time slots:_
  
  Contrary to the first thought to schedule lessons to classroom usage time slots, in this case it would make more sense to 
  see the classroom usage time slots as nurses and the lessons as shifts since we assign a resource (the usage time slot) to 
  some entity requiring an amount of this resource (the lessons). 
  We could then e.g. define that each shift (lesson) needs exactly one nurse (time slot) and each time slot can only contain one lesson (as a nurse can only work in one shift at the same time).
  
- _A coaching agency wants to schedule coaches to pupils_:
  
  This case can be approached from two sides:
  1. If we want to let the pupils define the time when they can take lessons and do not care about which coach teaches which pupil
     it makes sense to see the pupils as shifts which need covering by nurses (coaches).
  2. If we want to have the most compact schedule for all coaches it makes sense to see the pupils as nurses which are assigned
    to shifts (coaches) such that there are as less unnecessary breaks as possible.
     
- _A logistics agency for oversee transports want to schedule goods to their containers:_

  This case can also be approached from two sides:
  1. The agency want to get the most out of there containers and transport as many goods as possible. 
    Then it makes sense to see the containers as shifts and the goods as nurses and to maximize the number of nurses per shift.
  2. The agency lets customers decide when there goods should be shipped and then wants to schedule the goods to the ships and containers.
    In this case we can think of the container space as the resource to be scheduled to the goods requiring this space 
     and therefore it makes more sense to model the container space nurses and the goods as shifts.
     
This section was intended to show some use cases and approaches to demonstrate the broadness of the definitions of nurses and shifts
and to emphasise the broad range of problems which can be modeled as a NSP. 
In addition, it should be clear now that there is **not always only one way** to model a specific problem but it often heavily depends
on the goal to be achieved with the scheduling.

### Which constraints do you have?

The examples shown above indicate that all these implementations of problems do not make sense without some constraints between nurses and shifts,
nurses and nurses, a nurse poses etc.

We distinguish between **hard** and **soft** constraints:

#### Hard Constraints

Hard constraints define some real constraints, i.e. a condition that **must** be satisfied **for a solution to be valid**.
This means that any schedule not fulfilling any of these hard constraints is not considered a valid solution and needs correction.
Examples could be:

- Nurses may work at most 1 shift per day
- Shift A requires at least 2 nurses
- Nurses A must not be scheduled to the same shift as nurse B
- ...

#### Soft Constraints

Soft constraints define quality characteristics which define the quality of a given solution.
This quality may be influenced by

- some statistic property we want to maximize / minimize
- the compliance with nurses wishes
- some "hard constraints" we want to improve if possible (e.g. shift A requires at least 2 nurses but the more the better)
- ...

For **more information** on this topic please refer to the dedicated [constraints section](Constraints).

## Step-by-Step

This is a step-by-step guide demonstrating the use of this package by an example problem of scheduling nurses to shifts in a hospital.

### 1. The nurses class

If not already existent create a class representing the **nurses** and implement the `INurse` interface in this class.

```java
import com.gitlab.simonla.nspsolver.INurse;

public class myNurse implements INurse {
    private String name;
    private boolean senior;

    public myNurse(String name, boolean senior) {
        // Default constructor
    }

    // Default getters and setters
}
```
   
### 2. The shifts class

If not already existent create a class representing the **shifts** and implement the `IShift` interface in this class.

```java
import com.gitlab.simonla.nspsolver.INurse;import com.gitlab.simonla.nspsolver.IShift;import java.util.Locale;

public class myShift implements IShift {
    private LocalDate localDate;
    private int numberOfNurses;
    private boolean requiresSeniorNurse;
    private ArrayList<INurse> nurses;

    public myShift(LocalDate localDate, int numberOfNurses, boolean requiresSeniorNurse) {
        // Default constructor
    }

    // Default getters and setters
}
```

### 3. The constraints

If there are hard / soft constraints you need multiple times (with different parameters) you can define these in a separate class to 
be able to reuse them. If you have "one-time" constraints, you may as well use lambda expressions instead.

You can create constraints by implementing the `IHardConstraint` / `ISoftConstraint`:

```java
import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;

// Short hand constraint that nurse1, nurse2 and nurse3 **must** be scheduled together
public class scheduleTogether implements IHardConstraint {
    private INurse nurse1;
    private INurse nurse2;
    private INurse nurse3;

    public scheduleTogether(INurse nurse1, INurse nurse2, INurse nurse3) {
        // Some constructor
    }

    @Override
    public boolean isFulfilled(Solution solution) {
        for (IShift shift : solution.getSchedule())
            if (shift.contains(nurse1) || shift.contains(nurse2) || shift.contains(nurse3))
                if (!shift.contains(nurse1) || !shift.contains(nurse2) || !shift.contains(nurse3))
                    return False;
        return True;
    }
}
```

The most constraints can be modeled as one of five different constraint types:

1. nurse relation constraint
2. nurse assignment constraint
3. nurses hard constraint
4. shifts hard constraint
5. schedule constraint

An extensive explantion of these different constraint types can be found in the [constraints section](Constraints).

Therefore if your constraint matches any of this types, you can use these predefined types to save some work. 
To make use of these types you can implement the corresponding functional interface (e.g. `INurseRelationHardConstraintType`)
and provide the instance of this interface as a parameter of the corresponding class (e.g. `NurseRelationHardConstraint`):

```java
import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.soft.IShiftsSoftConstraintType;

// Short hand constraint to define specific shifts which need as much senior nurses as possible
public class someShiftsShouldHaveAsMuchSeniorsAsPossible implements IShiftsSoftConstraintType {
    @Override
    public double getFulfillmentRating(IShift shift, int lowerBound, int upperBound, Solution solution) {
        double rating = 0;
        for (INurse nurse: shift)
            if (nurse.isSenior())
                rating++;
        rating /= shift.getNurses().size();
        return rating;
    }
}
```

Of course there are various other ways of creating and defining constraints and various other and probably more useful constraints
but this examples should give an idea of how to use constraints.

An example on how to use this implementation of the functional interface and also on using a lambda instead are given in step 7.

### (4. The Nsp class)

The `Nsp` class represents the instance of the Nurse Scheduling Problem you are trying to solve. 
In **most** cases you can use the predefined class without modifications but if you need some additional functionality you can extend this class.\
The most common case for extending the `Nsp` class is probably to overwrite the `double getFitnessRating(Solution solution)`
method to implement another function to evaluate the fitness of a given solution.

### (5. The Solution class)

The `Nsp` class represents a solution to a given NSP, i.e. the resulting schedule. 
In **most** cases you can use the predefined class without modifications but if you need some additional functionality you can extend this class.

### 6. The Solver

To solve the given NSP instance you need some solving algorithm.
Therefore, create a class containing your solving algorithm and implement the `INspSolver` interface in this class.\
You can use the `boolean isValid(Solution solution)` and `double getFitnessRating(Solution solution)` methods of the `Nsp` class
for your solving algorithm.

_In future there will be some default solving algorithms included in this package._

```java
import com.gitlab.simonla.nspsolver.Nsp;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.solver.INspSolver;

public class mySolver implements INspSolver {
    @Override
    public Solution solveNsp(Nsp nsp) {
        return solveNsp(nsp, 0.5);
    }

    @Override
    public Solution solveNsp(Nsp nsp, double minFitnessBoundary) {
        // Some fancy solving logic (e.g. a genetic algorithn, simulated annealing, swarm optimization etc.)
    }
}
```

### 7. Create your objects and solve

The last step is to create all your nurses, shifts, constraints, assign them to the Nsp and solve this with your solver:

```java
import com.gitlab.simonla.nspsolver.INurse;
import com.gitlab.simonla.nspsolver.IShift;
import com.gitlab.simonla.nspsolver.Nsp;
import com.gitlab.simonla.nspsolver.Solution;
import com.gitlab.simonla.nspsolver.constraints.IHardConstraint;
import com.gitlab.simonla.nspsolver.constraints.ISoftConstraint;
import com.gitlab.simonla.nspsolver.constraints.hard.NurseAssignmentHardConstraint;
import com.gitlab.simonla.nspsolver.constraints.soft.ShiftsSoftConstraint;
import com.gitlab.simonla.nspsolver.solver.INspSolver;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {
  public static void main(String[] args) {
    ArrayList<INurse> nurses = new ArrayList<>();

    INurse nurse1 = new myNurse('Anna', true);
    INurse nurse2 = new myNurse('Bob', false);
    INurse nurse3 = new myNurse('Charlie', true);
    INurse nurse4 = new myNurse('Dolores', false);

    nurses.add(nurse1);
    nurses.add(nurse2);
    nurses.add(nurse3);
    nurses.add(nurse4);

    ArrayList<IShift> shifts = new ArrayList<>();

    IShift shift1 = new myShift(LocalDate.of(2021, 4, 3), 3, true);
    IShift shift2 = new myShift(LocalDate.of(2021, 4, 4), 2, true);
    IShift shift3 = new myShift(LocalDate.of(2021, 4, 6), 3, false);

    shifts.add(shift1);
    shifts.add(shift2);
    shifts.add(shift3);

    ArrayList<IHardConstraint> hardConstraints = new ArrayList<>();

    hardConstraints.add(new scheduleTogether(nurse1, nurse2, nurse4));
    // Use lambda to **not** assign nurse1 to shift2
    hardConstraints.add(new NurseAssignmentHardConstraint(nurse1, shift2, false, (n, s, b, so) -> !s.getNurses().contains(n)));

    ArrayList<ISoftConstraint> softConstraints = new ArrayList<>();

    // Use implementation of functional interface as evaluation function as defined above
    softConstraints.add(new ShiftsSoftConstraint(shift3, 0, new someShiftsShouldHaveAsMuchSeniorsAsPossible()));

    // Create the instance of the NSP
    Nsp nsp = new Nsp(shifts, nurses, hardConstraints, softConstraints);

    // Create your fancy solver
    INspSolver solver = new mySolver();

    // Finally solve the NSP
    Solution solution = mySolver.solve(nsp);
  }
}

```
