This guide describes the setup and usage of this maven package. 
To be able to use this package you have to use maven as your java project management software tool.

## Setup

#### Registry Setup
Add the following lines to your `pom.xml` to access this projects gitlab package registry:

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/24569933/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/24569933/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/24569933/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

#### Installation
With this being added you will be able to add the following in your `pom.xml` `dependencies` block to include the package:
```xml
<dependency>
  <groupId>com.gitlab.simonla</groupId>
  <artifactId>nspsolver</artifactId>
  <version>x.y.z</version>
</dependency>
```
Of course `x.y.z` have to be replaced with your required version number.
