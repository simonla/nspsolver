## Problem to solve

(Describe the problem you wish to see solved. What is its use case? What are the benefits?)

## Proposal

(If any, describe your proposal of how to tackle this problem. How would you like to see this issue be resolved?)

## Example Project

(If possible, please create an example project that demonstrates the use case, and link to it here.)

## Relevant code snippets / screenshots

(Paste any relevant code snippets / screenshots - please use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## What does success look like?

(Describe what must happen for you to mark this issue as resolve.)

/label ~enhancement ~suggestion
