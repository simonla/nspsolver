stages:
  - test
  - pre_release
  - release
  - docs
  - deploy

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task

image: maven:3.6-openjdk-15

cache:
  paths:
    - .m2/repository

sonarcloud-check:
  image: maven:3.6.3-openjdk-15
  stage: test
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify sonar:sonar
  only:
    - merge_requests
    - master
    - develop

include:
  - template: Code-Quality.gitlab-ci.yml

code_quality:
  image: docker:latest
  artifacts:
    paths: [gl-code-quality-report.json]
  tags:
    - dind

test_jdk15:
  stage: test
  script:
    - mvn $MAVEN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report
    - cat target/site/jacoco/index.html
  artifacts:
    when: always
    paths:
      - target/site/jacoco/
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml

semantic_release_version:
  image: node:13
  stage: pre_release
  before_script:
    - npm install @semantic-release/gitlab @semantic-release/changelog @semantic-release/exec
  script:
    - touch REL_VERSION.txt
    - npx semantic-release --dryRun
  artifacts:
    paths:
      - REL_VERSION.txt
  needs: [ ]
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

maven_release_version:
  stage: pre_release
  script:
    - mvn help:evaluate -Dexpression=project.version -q -DforceStdout > MVN_VERSION.txt
  artifacts:
    paths:
      - MVN_VERSION.txt
  needs: [ ]
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

maven_release:
  stage: release
  before_script:
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    - |
      if [[ $(grep -oP '\d\.\d\.\d' REL_VERSION.txt) ]]; then
        if [ "$(grep -oP '\d\.\d\.\d' REL_VERSION.txt)" != "$(grep -oP '\d\.\d\.\d' MVN_VERSION.txt)" ]; then
          mvn $MAVEN_CLI_OPTS versions:set -DnewVersion=$(grep -oP '\d\.\d\.\d' REL_VERSION.txt)
          git commit -a -m "ci: bump version in maven according to sematic release"
          git push -o ci.variable="BUMP_TO_SNAPSHOT=true" "https://${GITLAB_USER_NAME}:${GITLAB_TOKEN}@${CI_REPOSITORY_URL#*@}" "HEAD:${CI_COMMIT_BRANCH}"
        fi
      fi
  needs:
    - maven_release_version
    - semantic_release_version
    - test_jdk15
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

semantic_release:
  image: node:13
  stage: release
  before_script:
    - npm install @semantic-release/gitlab @semantic-release/changelog @semantic-release/exec
  script:
    - if [ "$(grep -oP '\d\.\d\.\d' REL_VERSION.txt)" == "$(grep -oP '\d\.\d\.\d' MVN_VERSION.txt)" ]; then npx semantic-release; fi
  artifacts:
    paths:
      - CHANGELOG.md
  needs:
    - maven_release_version
    - semantic_release_version
    - test_jdk15
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

generate_javadocs:
  stage: docs
  script:
    - mvn $MAVEN_CLI_OPTS javadoc:javadoc
  artifacts:
    paths:
      - target/site/apidocs
  needs: []

deploy:
  stage: deploy
  script:
    - mvn $MAVEN_CLI_OPTS deploy -s ci_settings.xml
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d\.\d\.\d$/'

deploy_to_snapshot:
  stage: deploy
  before_script:
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    - mvn $MAVEN_CLI_OPTS versions:set -DnewVersion="$(grep -oP '\d\.\d\.\d' MVN_VERSION.txt)-SNAPSHOT"
    - 'git commit -a -m "ci: bump version in maven to snapshot"'
    - git push -o "https://${GITLAB_USER_NAME}:${GITLAB_TOKEN}@${CI_REPOSITORY_URL#*@}" "HEAD:${CI_COMMIT_BRANCH}"
  only:
    variables:
      - $BUMP_TO_SNAPSHOT

pages:
  image: python:3.7
  stage: deploy
  before_script:
    - pip install anybadge
  script:
    - mkdir .public
    - anybadge -l version -v $(git describe --abbrev=0 --tags) -c green -f version.svg
    - cp version.svg .public
    - cp -r target/site/apidocs/* .public
    - mv .public public
  artifacts:
    paths:
      - public
  dependencies:
    - generate_javadocs
    - maven_release_version
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

deploy_wiki:
  image: ubuntu:latest
  stage: deploy
  before_script:
    - apt update -yq && apt install -yq git
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    - |
      if [ -d wiki ]; then
        git clone "https://${GITLAB_USER_NAME}:${GITLAB_TOKEN}@${CI_WIKI_URL}"
        rm -rf nspsolver.wiki/*
        cp -r wiki/* nspsolver.wiki
        cd nspsolver.wiki
        git add .
        output=$(git commit -a -m 'Update wiki')
        if [[ ! $(echo "$output" | grep -oP 'nothing to commit') ]]; then
          git push "https://${GITLAB_USER_NAME}:${GITLAB_TOKEN}@${CI_WIKI_URL}" "HEAD:master"
        fi
      fi
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d\.\d\.\d$/'
