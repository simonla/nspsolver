# NSPSolver

[![pipeline status](https://gitlab.com/simonla/nspsolver/badges/master/pipeline.svg)](https://gitlab.com/simonla/nspsolver/-/commits/master) 
[![coverage report](https://gitlab.com/simonla/nspsolver/badges/master/coverage.svg)](https://gitlab.com/simonla/nspsolver/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=simonla_nspsolver&metric=alert_status)](https://sonarcloud.io/dashboard?id=simonla_nspsolver)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![version](https://simonla.gitlab.io/nspsolver/version.svg)](https://gitlab.com/simonla/nspsolver/-/releases/)

A maven package implementing various algorithms to solve the [Nurse Scheduling Problem](https://en.wikipedia.org/wiki/Nurse_scheduling_problem) (also known as the Nurse Rostering Problem).

## Where can I find additional information?

- In the projects **[Wiki](https://gitlab.com/simonla/nspsolver/-/wikis/home)** page
- Read the **[javadocs](https://simonla.gitlab.io/nspsolver)**
- Read the changelogs on the **[releases page](https://gitlab.com/simonla/nspsolver/-/releases)**
- Submit **[an issue](https://gitlab.com/simonla/nspsolver/-/issues/new?issue)**

## When do I need this package?

You are programming something in java (with maven) and encounter a situation where you want to **schedule** some objects
to some jobs and you have some constraints that have to be fulfilled or some criteria which define whether a schedule is a good
or a bad schedule. And you begin to wonder how to actually schedule these objects e.g. in a fair way and start implementing 
some algorithm randomly assigning objects to jobs whilst fulfilling your constraints. And the more constraints you add
the more complex your algorithm gets and you start to wonder, if there's a more **systematic approach** to this topic.

Then this package is what you're searching for!

## What does this package provides?

This package provides (_italic_ = still in development):

- basic interfaces for a systematic approach
- an easy way to create soft and hard constraints
- _some default soft and hard constraints capturing the most common use case_
- _multiple algorithms to find a nearly optimal solution to the problem_

## May I contribute?

Of course! Bug reports, feature request and questions are highly appreciated. You may also request access to this repo.
